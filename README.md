#  List of Colleges with Technical Departments in Delhi/NCR
- Aim of this repo is to make it easy to find communities and people to contact. 
- This will make easier to start university level free software movement.
- Feel free to contribute and add communities to it.

### Contents:
- [Universities with related society present](#universities-with-related-society-present)
- [University without related society(Or we dont know their presence)](#universities-without-related-society)

# Universities with Related Society Present

## **University Name: Jaypee Institute of Information Technology, Sector 128**
#### University Website: https://www.jiit.ac.in
#### Community Name: JIIT Open Dev Circle - JODC
#### Community Website:
#### IRC: https://discord.gg/YMFm7xs 
#### Contact Person: 
- Ankit Khandelwal (@ankk98) (ankk98 [at] protonmail [dot] com)
#### Other Links: 
- https://www.facebook.com/groups/jiitodc/ 
- https://twitter.com/jiitodc

## **University Name: Jaypee Institute of Information Technology, Sector 62**
#### University Website: https://www.jiit.ac.in
#### Community Name: Open Source Developers Community
#### Community Website: 
#### IRC: https://t.me/jiitosdc
#### Contact Person: 
- Name (example [at] protonmail [dot] com)
#### Other Links: 
- https://www.facebook.com/groups/jiitlug/

## **University Name: Amity University**
#### University Website: 
#### Community Name: Alias
#### Community Website: http://asetalias.in/
#### IRC: 
#### Contact Person: 
- 
#### Other Links: 
- https://www.facebook.com/asetalias/

## **University Name: Name**
#### University Website: 
#### Community Name: 
#### Community Website:
#### IRC: 
#### Contact Person: 
- Name (example [at] protonmail [dot] com)
#### Other Links: 


# Universities without related society
(Or we dont know their presence)

### **University Name: **
#### University Website:
#### Contact Person: 
- Name (example [at] protonmail [dot] com)
#### Other Links: